
-- ARTiVIS video streaming daemon -- unit test suite
-- Copyright (C) 2013  Pedro Ângelo <pangelo@void.io>
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 2 of the License, or (at
-- your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 

luaunit = require('luaunit')

-- work around non-namespaced version of luaunit
if type(luaunit) == 'boolean' then
   luaunit = {}
   luaunit.assertEquals = assertEquals
   luaunit.LuaUnit = LuaUnit
end

TestStreamdInternal = {}

function setup_environment()

end


function TestStreamdInternal:test_defaults()
   print ('test default settings')
   assert (streamd)
   assert (streamd.options)
   luaunit.assertEquals (streamd.options.daemon, false)
   luaunit.assertEquals (streamd.options.config_filename, '/etc/artivis/streamd.conf')
   luaunit.assertEquals (streamd.options.pid_filename, '/var/run/streamd.pid')
   luaunit.assertEquals (streamd.options.log_filename, '/var/log/streamd.log')
end


function TestStreamdInternal:test_config()
   print ('test config parsing')
   local default_config_filename = streamd.options.config_filename
   streamd.options.config_filename = '../conf/streamd.conf.sample'
   local config = streamd:load_config()
   assert (streamd.config.pipeline)
   luaunit.assertEquals (streamd.config.pipeline.source, 'video')
   luaunit.assertEquals (streamd.config.pipeline.codec, 'mjpeg')
   luaunit.assertEquals (streamd.config.pipeline.output, 'raw')
   assert (streamd.config.source)
   assert (streamd.config.source.file)
   luaunit.assertEquals (streamd.config.source.file.filename, 'test.ogg')
   assert (streamd.config.source.audio)
   luaunit.assertEquals (streamd.config.source.audio.device, 'hw:0')
   luaunit.assertEquals (streamd.config.source.audio.channels, 1)
   luaunit.assertEquals (streamd.config.source.audio.samplerate, 22100)
   assert (streamd.config.source.video)
   luaunit.assertEquals (streamd.config.source.video.device, '/dev/video0')
   luaunit.assertEquals (streamd.config.source.video.format, 'video/x-raw-yuv')
   luaunit.assertEquals (streamd.config.source.video.framerate, '30/1')
   luaunit.assertEquals (streamd.config.source.video.width, 640)
   luaunit.assertEquals (streamd.config.source.video.height, 480)
   luaunit.assertEquals (streamd.config.source.video.brightness, 0)
   luaunit.assertEquals (streamd.config.source.video.contrast, 0)
   luaunit.assertEquals (streamd.config.source.video.saturation, 0)
   luaunit.assertEquals (streamd.config.source.video.hue, 0)
   assert (streamd.config.source.raspicam)
   assert (streamd.config.codec)
   assert (streamd.config.codec.mjpeg)
   luaunit.assertEquals (streamd.config.codec.mjpeg.bitrate, 300000)
   assert (streamd.config.codec.ogg)
   luaunit.assertEquals (streamd.config.codec.ogg.quality, 3)
   assert (streamd.config.codec.webm)
   luaunit.assertEquals (streamd.config.codec.webm.quality, 5)
   assert (streamd.config.output)
   assert (streamd.config.output.raw)
   luaunit.assertEquals (streamd.config.output.raw.host, '127.0.0.1')
   luaunit.assertEquals (streamd.config.output.raw.port, 9999)
   assert (streamd.config.output.file)
   luaunit.assertEquals (streamd.config.output.file.maxsize, 2147483648)
   luaunit.assertEquals (streamd.config.output.file.location, 'output-%05d.mjpeg')
   assert (streamd.config.output.snapshot)
   luaunit.assertEquals (streamd.config.output.snapshot.interval, 60)
   luaunit.assertEquals (streamd.config.output.snapshot.format, 'jpg')
   luaunit.assertEquals (streamd.config.output.snapshot.location, 'snapshots/snapshot-%05d.jpg')
   assert (streamd.config.output.icecast)
   luaunit.assertEquals (streamd.config.output.icecast.ip, 'giss.tv')
   luaunit.assertEquals (streamd.config.output.icecast.port, 8000)
   luaunit.assertEquals (streamd.config.output.icecast.username, 'source')
   luaunit.assertEquals (streamd.config.output.icecast.password, 'your-gisstv-password')
   luaunit.assertEquals (streamd.config.output.icecast.mount, 'your-gisstv-mountpoint')
   streamd.options.config_filename = default_config_filename
end


function TestStreamdInternal:test_pipeline_cam_mjpeg_raw()
   print ('testing common config: mjpeg camera local stream')
   local default_config = streamd.config
   streamd.config = {
      pipeline = {
	 source = 'video',
	 codec = 'pass',
	 output = 'raw'
      },
      source = {
	 video = {
	    device = '/dev/video0',
	    format = 'image/jpeg',
	    framerate = '30/1',
	    width = 640,
	    height = 480,
	    brightness = 0,
	    contrast = 0,
	    saturation = 0,
	    hue = 0
	 }
      },
      output = {
	 raw = {
	    host = '127.0.0.1',
	    port = 9999
	 }
      }
   }

   streamd:create_pipeline_elements()

   assert (streamd.pipeline)

   -- cleanup before next test
   streamd.pipeline = nil
   streamd.config = default_config 
end


function TestStreamdInternal:test_pipeline_cam_ogg_icecast()
   local default_config = streamd.config
   streamd.config = {
      pipeline = {
	 source = 'video',
	 codec = 'ogg',
	 output = 'icecast'
      },
      source = {
	 video = {
	    device = '/dev/video0',
	    format = 'image/jpeg',
	    framerate = '30/1',
	    width = 640,
	    height = 480,
	    brightness = 0,
	    contrast = 0,
	    saturation = 0,
	    hue = 0
	 }
      },
      codec = {
	 ogg = {
	    quality = 3
	 }
      },
      output = {
	 icecast = {
	    ip = 'giss.tv',
	    port = 8000,
	    username = 'source',
	    password = 'your-gisstv-password',
	    mount = 'your-gisstv-mountpoint'
	 }
      }
   }

   streamd:create_pipeline_elements()
   
   assert (streamd.pipeline)

   if streamd.pipeline then
      streamd.pipeline = 'NULL'
   end

   streamd.config = default_config 
end

luaunit.LuaUnit.run('TestStreamdInternal')
