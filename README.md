streamd - a simple media streaming server script
================================================

streamd is a small Lua script that takes care of creating and running a Gstreamer pipeline for media streaming as a background process. It reads the pipeline description from a text-based configuration file and allows for remote control of this pipeline through a UNIX socket.

It is especially useful in embedded computers like the Raspberry Pi to run streaming servers on boot without having access to a keyboard or screen.

Requirements
------------

 * lua5.2
 * lua-lgi
 * lua-posix
 * lua-socket
 * gstreamer1.0-plugins-base
 * gstreamer1.0-plugins-good
 * gstreamer1.0-libav
 * gir1.2-gst-plugins-base-1.0
 * gir1.2-gstreamer-1.0

Building and Installing
-----------------------

 * clone this repository
 * install dependencies
 * install `make`
 * run `make` in this repository
 * optionally run `make install`


