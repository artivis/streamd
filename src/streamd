#! /usr/bin/env lua

-- ARTiVIS video streaming daemon
-- Copyright (C) 2013  Pedro Ângelo <pangelo@void.io>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 2 of the License, or (at
-- your option) any later version.
--
-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

--
-- Library loading
--

-- system libraries
local os = require('os')
local posix = require('posix')
local socket = require('socket.unix')

-- gstreamer library interface
local lgi = require 'lgi'
local Gst = lgi.require('Gst', '1.0')

--
-- Application data space
--

local streamd = {}

-- server version
streamd.version = '1.1'

-- default state
streamd.options = {}
streamd.options.daemon = false
streamd.options.config_filename = '/etc/streamd.conf'
streamd.options.pid_filename = '/var/run/streamd.pid'
streamd.options.log_filename = '/var/log/streamd.log'

-- video format translation table
streamd.video_formats = {
   ['yuv'] = {
      name = 'video/x-raw,format=YUV',
      allow = {'pass', 'mjpeg', 'ogg', 'webm'},
      convert = {'mjpeg', 'ogg', 'webm'}
   },
   ['yuy2'] = {
      name = 'video/x-raw,format=YUY2',
      allow = {'pass', 'mjpeg', 'ogg', 'webm'},
      convert = {'mjpeg', 'ogg', 'webm'}
   },
   ['jpeg'] = {
      name = 'image/jpeg',
      allow = { 'pass' },
      convert = {}
   }
}

--
-- pipeline construction
--

streamd.create_pipeline_elements =
   function (self)
      local elements = {}

      if self.config.pipeline.source == 'video' then
	 local source = Gst.ElementFactory.make ('v4l2src')
	 source.device = self.config.source.video.device
	 source.brightness = self.config.source.video.brightness
	 source.contrast = self.config.source.video.contrast
	 source.saturation = self.config.source.video.saturation
	 source.hue = self.config.source.video.hue
	 table.insert (elements, source)

	 local filter = Gst.ElementFactory.make ('capsfilter')
	 local video_opts = self.config.source.video
	 local format_opts = streamd.video_formats[video_opts.format]
	 local caps_str = string.format ('%s, framerate=%s, width=%d, height=%d',
					 format_opts.name,
					 video_opts.framerate,
					 video_opts.width,
					 video_opts.height)
	 filter.caps = Gst.Caps.from_string (caps_str)
	 table.insert (elements, filter)
      else
	 -- FIXME: implement 'file', 'audio' and 'raspicam' sources
	 streamd:log ('critical',
		      "support for pipeline source '" ..
			 self.config.pipeline.source ..
			 "' is not implemented")
	 streamd:shutdown()
      end

      if not streamd:codec_allowed() then
	 local format = self.config.source.video.format
	 streamd:log ('critical',
		      string.format("codec '%s' is not allowed in this pipeline. ",
				    self.config.pipeline.codec) ..
		      string.format("Video format '%s' allows: %s",
				    format,
				    table.concat(streamd.video_formats[format].allow, ', ')))
	 streamd:shutdown()
      end
	 
      if streamd:codec_needs_conversion() then
	 local converter = Gst.ElementFactory.make ('videoconvert')
	 table.insert(elements, converter)
      end
      
      if self.config.pipeline.codec == 'mjpeg' then
	 local encoder = Gst.ElementFactory.make ('avenc_mjpeg')
	 encoder.bitrate = self.config.codec.mjpeg.bitrate
	 table.insert (elements, encoder)
      elseif self.config.pipeline.codec == 'ogg' then
	 local encoder = Gst.ElementFactory.make ('theoraenc')
	 encoder.quality = self.config.codec.ogg.quality
	 local muxer = Gst.ElementFactory.make ('oggmux')
	 table.insert (elements, encoder)
	 table.insert (elements, muxer)
      elseif self.config.pipeline.codec == 'webm' then
	 local encoder = Gst.ElementFactory.make ('vp8enc')
	 encoder.cq_level = self.config.codec.webm.quality
	 table.insert (elements, encoder)
      elseif self.config.pipeline.codec ~= 'pass' then
	 streamd:log ('critical',
		      "support for pipeline codec '" ..
			 self.config.pipeline.codec ..
			 "' is not implemented")
	 streamd:shutdown()
      end
      
      local queue = Gst.ElementFactory.make ('queue')
      table.insert (elements, queue)

      if self.config.pipeline.output == 'raw' then
	 local output = Gst.ElementFactory.make ('tcpserversink')
         self.output = output
	 output.host = self.config.output.raw.host
	 output.port = self.config.output.raw.port
         output.buffers_soft_max = 10
         output.recover_policy = 1
	 table.insert (elements, output)
      elseif self.config.pipeline.output == 'file' then
	 local output = Gst.ElementFactory.make ('filesink')
	 local savedir = posix.dirname (self.config.output.file.location)
	 local files, errstr, errno = posix.dir (savedir)
	 if errno == 2 then
	    index = 0
	    streamd:log ('critical', 'output dir not found: ' .. savedir)
	    streamd:shutdown()
	 end
	 local index = 0
	 local template = self.config.output.file.location
	 repeat
	    local fname = string.format (template, index)
	    local status, errstr, errno = posix.access (fname, 'w')
	    index = index + 1
	 until status == nil
	 output.location = string.format (template, index - 1)
	 output.buffer_mode = 0
	 local mux = Gst.ElementFactory.make ('avimux')
	 table.insert (elements, mux)
	 table.insert (elements, output)
      elseif self.config.pipeline.output == 'icecast' then
	 local output = Gst.ElementFactory.make ('shout2send')
	 output.ip = self.config.output.icecast.ip
	 output.port = self.config.output.icecast.port
	 output.username = self.config.output.icecast.username
	 output.password = self.config.output.icecast.password
	 output.mount = self.config.output.icecast.mount
	 table.insert (elements, output)
      else
	 streamd:log ('critical',
		      "support for pipeline output '" ..
			 self.config.pipeline.output ..
			 "' is not implemented")
	 streamd:shutdown()
      end

      self:create_pipeline()

      -- add all elements to pipeline
      for _, el in ipairs(elements) do
	 streamd:log('info', 'adding pipeline element: ' .. el.name)
	 self.pipeline:add(el)
      end

      -- connect al elements
      for i=1, #elements - 1, 1 do
	 elements[i]:link(elements[i+1])
      end

   end

streamd.codec_allowed =
   function(self)
      if self.config.pipeline.source ~= 'video' then return true end
      local format = self.config.source.video.format
      local codec = self.config.pipeline.codec
      return table.contains(streamd.video_formats[format].allow, codec)
   end

streamd.codec_needs_conversion =
   function (self)
      if self.config.pipeline.source ~= 'video' then return false end
      local format = self.config.source.video.format
      local codec = self.config.pipeline.codec
      return table.contains(streamd.video_formats[format].convert, codec)
   end

streamd.create_pipeline =
   function (self)
      -- create pipeline if it doesn't exist
      if not self.pipeline then
	 self.pipeline = Gst.Pipeline.new ('artivis-stream')
	 self.pipeline.state = 'NULL'
      end
   end

streamd.restart_pipeline =
   function(self)
      streamd:log('info', 'sleeping a bit before attempting to restart pipeline')
      posix.sleep(10) -- TODO: make this a config option
      streamd:create_pipeline()
      streamd.pipeline.state = 'PLAYING'
   end

streamd.load_config =
   function (self)
      local f = io.open(self.options.config_filename, 'r')
      if not f then
	 streamd:log('critical', 'cannot open config file: ' .. self.options.config_filename)
	 streamd:shutdown()
	 return
      end
      self.config = ltin_parse(f:read('*a'))
      f:close()
   end

streamd.log_severity_id = {
   ['alert'] = 1,
   ['critical'] = 2,
   ['error'] = 3,
   ['warning'] = 4,
   ['notice'] = 5,
   ['info'] = 6
}

streamd.log_stdout =
   function (self, severity, msg)
      print (self:log_timestamp() .. ' [' .. severity .. ']: ' .. msg)
   end

streamd.log_syslog =
   function (self, severity, msg)
      posix.syslog (self.log_severity_id[severity], msg)
   end

streamd.log_file_open =
   function (self, filename)
      self.log_fd = io.open (filename, 'w')
      self.log_fd:setvbuf('line')
   end

streamd.log_timestamp =
   function (self)
      local t = posix.gmtime(posix.time())
      return string.format('%s/%s/%s %s:%s',
			   t.year, t.month, t.monthday, t.hour, t.min)
   end

streamd.log_file =
   function (self, severity, msg)

      self.log_fd:write(self:log_timestamp() .. ' [' .. severity .. ']: ' .. msg ..'\n')
   end

streamd.log = streamd.log_stdout

streamd.shutdown =
   function (self)
      streamd:log ('info', 'cleaning up before normal shutdown')

      if streamd.pipeline then
	 streamd.pipeline.state = 'NULL'
      end

      -- close log file
      if self.log_fd then
	 self.log_fd:close()
      end
      -- TODO: remove pid file
      posix._exit(0)
   end

--
-- Helper functions
--

-- Parse LTIN markup by reusing the lua parser in a sandbox
-- (from: https://github.com/luvit/ltin/)
function ltin_parse(string)
   local sandbox = {}
   local fn, message = load("return " .. string, 'ltin_parse', 't', sandbox)
   if not fn then error(message) end
   return fn()
end

-- run unit tests if required
if  arg[1] == 'test' then
   dofile('../test/internal.lua')
   os.exit(0)
end

-- check if a table contains a value
function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

---
--- Server initialization
---

streamd:log('info', string.format('streamd v%s starting', streamd.version))

-- check command line options
local short_opts = 'hc:dl:p:'

for r, optarg, optind in posix.getopt (arg, short_opts) do
   if r == 'h' then
      print (string.format ([[

    ARTiVIS streaming daemon v%s

    This help:     streamd [-h]
    Run in tty:    streamd [-c <filename>]
    Run as daemon: streamd [-c <filename>] -d [-p <filename>] [-l <filename>]

    Options:
       -h    This help
       -c    Configuration file to use
       -d    Try to run as a daemon
       -p    Pid file to use (only valid if -d is also specified)
       -l    Log file to use (only valid if -d is also specified)

      ]], streamd.version))
      os.exit(0)
   elseif r == 'c' then
      streamd.options.config_filename = optarg
   elseif r == 'd' then
      streamd.options.daemon = true
   elseif r == 'p' then
      streamd.options.pid_filename = optarg
   elseif r == 'l' then
      streamd.options.log_filename = optarg
   end
end

streamd:load_config()

if streamd.options.daemon then

   -- close console file descriptors
   for fd = 0, 2, 1 do
      posix.close(fd)
   end

   -- setup umask
   posix.umask(0) -- FIXME, should not be zero

   -- open logfile
   streamd:log_file_open(streamd.options.log_filename)
   streamd.log = streamd.log_file

   -- chdir to /
   posix.chdir('/')

   -- create pid file
   -- TODO

   -- setup signal handlers
   -- SIGTTOU, SIGTTIN, SIGTSTP are related to background job control
   posix.signal(posix.SIGTTOU, posix.SIG_IGN);
   posix.signal(posix.SIGTTIN, posix.SIG_IGN);
   posix.signal(posix.SIGTSTP, posix.SIG_IGN);

   -- user interrups
   posix.signal(posix.SIGINT, posix.SIG_IGN)

   -- "hangups"
   posix.signal(posix.SIGHUP, posix.SIG_IGN)
   posix.signal(posix.SIGQUIT, posix.SIG_IGN)

   -- graceful shutdown signal
   posix.signal(posix.SIGTERM, function() streamd:shutdown() end)
else
   -- make user interrupt shutdown gracefully
   posix.signal(posix.SIGINT, function() streamd:shutdown() end)
end

-- SIGPIPE can be raised when the pipeline attempts to write to
-- a closed socket.
posix.signal(posix.SIGPIPE, posix.SIG_IGN)

-- create pipeline
streamd:create_pipeline_elements()
streamd.pipeline.state = 'PLAYING'

--
-- Main loop
--

while true do

   -- poll for messages on the pipeline bus
   local message = streamd.pipeline.bus:pop()
   while message do
      if message.type.ERROR then
   	 streamd:log('error', message:parse_error().message)
	 streamd.pipeline.state = 'NULL'
	 streamd:restart_pipeline()
      elseif message.type.EOS then
   	 streamd:log('info', 'end of stream')
	 streamd.pipeline.state = 'NULL'
	 streamd:restart_pipeline()
      elseif message.type.STATE_CHANGED then
   	 local old, new, pending = message:parse_state_changed()
   	 streamd:log('info', string.format('state changed: %s->%s:%s', old, new, pending))
      else
	 streamd:log('warning', 'unhandled message')
      end
      message = streamd.pipeline.bus:pop()
   end

   -- FIXME: properly poll control socket, this is just for testing.
   posix.sleep(1)
end

pipeline.state = 'NULL'
