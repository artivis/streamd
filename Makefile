
all: streamd

streamd: src/streamd
	echo "nothing to do"

clean:
	-rm -r *~

install:
	install -o root -m 755 src/streamd /usr/bin
	install -o root -m 600 conf/streamd.conf.sample /etc/streamd.conf
	install -o root -m 755 init/streamd /etc/init.d
	update-rc.d streamd defaults

test:
	cd src && ./streamd test

.PHONY: clean install test
